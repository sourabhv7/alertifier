from flask import Flask, render_template, request, session, redirect
from datetime import datetime
import json
from random import randint
import random
from bson.objectid import ObjectId
import os
from werkzeug.utils import secure_filename
import math
import bs4
import urllib.request
from random import randint
import smtplib
import gunicorn
from flask_bcrypt import Bcrypt
from mongoengine import connect
from models import Clients, Products, Users, Contact,Deals


def get_price(soup):
    try:
        prices = soup.find(id="priceblock_ourprice").get_text()
        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        
    except:
        pass

    try:
        prices = soup.find(id="priceblock_dealprice").get_text()
        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        
    except:
        pass
    try:
        prices = soup.find(id="priceblock_saleprice").get_text()
        # print(prices)
        # print('by priceblock_saleprice')
        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        
    except:
        price = "Currently not available or out of stock."
        return price


def func(link):
    
    ran_v = random.uniform(1.0,6.0)
    ran_v = str(ran_v)
    header = {'User-Agent': f'Mozilla/{ran_v}'}
    req = urllib.request.Request(link, headers=header)
    resp = urllib.request.urlopen(req)
    data = resp.read()
    sauce = data.decode("UTF-8")


    soup = bs4.BeautifulSoup(sauce, "html.parser")
    if 'amazon' in link:
        price = get_price(soup)
        
        asin = soup.find(id="ASIN")
        asin = asin.get('value')
        asin = str(asin)
        #print(asin)

        title = soup.find(id="productTitle").get_text()
        title = title.strip()
        #print(title)

        image = soup.find(id="imgTagWrapperId")
        image = image.find('img')

        image = image["src"]
        graphImg = f"https://graph.keepa.com/pricehistory.png?asin={asin}&domain=in&width=800&height=250&range=120&cFont=000000"
        #print(graphImg)
        data = {
            'amazon': True,              #
            'flipkart': False,             #
            'price': price,
            'title': title,                  #
            'graphImg': graphImg,
            'image': image,
            'prod_link': 'https://www.amazon.in/dp/'+str(asin)  #
        }
    elif 'flipkart' in link:
        price = soup.find(class_="_30jeq3 _16Jk6d").get_text()
        price = float(price .replace("₹", "").replace(",", ""))
        # print(price)

        title = soup.find(class_="B_NuCI").get_text()
        # print(title)

        try:
            image = soup.find(class_="_396cs4 _2amPTt _3qGmMb _3exPp9")
            image = image['src']
        except:
            image = soup.find(class_="_2r_T1I _396QI4")
            image = image['src']
        data = {
            'flipkart': True,
            'amazon': False,
            'price': price,
            'title': title,
            'image': image,
            'prod_link': link
        }
    else:
        data = {
            # 'title' : 'Unable to find product or Invalid product link.',
            'flipkart': False,
            'amazon': False
        }
    return data


def send_mail(to, name, otp):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    try:

        msg = """From: Alertifier <alertifier.ml@gmail.com>
To: """+name+""" <"""+to+""">
Subject: Alertifier One Time Password        
Hi """+name+"""
Your one time password is """ + otp

        server.starttls()
        server.login('alertifier.ml@gmail.com', params['gmail_pass'])
        server.sendmail('alertifier.ml@gmail.com', to, msg)
        response = {
            'status': 'success',
            'from': 'alertifier.ml@gmail.com',
            'to': to,
            'message': msg
        }
        return response
    except:
        response = {
            'status': 'failed'
        }
        return response

def contact_mail(to, name):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    try:

        msg = """From: Alertifier <alertifier.ml@gmail.com>
To: """+name+""" <"""+to+""">
Subject: Thanks for contacting Alertifier     
Hi """+name+"""
Thank you for contacting Alertifier. Our team will be contact you shortly. """

        server.starttls()
        server.login('alertifier.ml@gmail.com', params['gmail_pass'])
        server.sendmail('alertifier.ml@gmail.com', to, msg)
        response = {
            'status': 'success',
            'from': 'alertifier.ml@gmail.com',
            'to': to,
            'message': msg
        }
        return response
    except:
        response = {
            'status': 'failed'
        }
        return response

# Global Variale
is_adding = False


with open('config.json', 'r') as c:
    params = json.load(c)["params"]

local_server = False

if local_server:
    uri = params['local_uri']
else:
    uri = params['prod_uri']

app = Flask(__name__)
app.secret_key = 'my-secret-key'
bcrypt = Bcrypt(app)

# Connecting mongoengine with mongoDB server instance
connect(host=uri)



@app.route("/", methods=['GET', 'POST'])
def home():
    #getting client info
    ip = request.remote_addr
    header = request.user_agent
    header = str(header)

    if not Clients.objects(ip = ip, header = header):
        Clients(ip = ip, header = header).save()
    
    # if not mongo.db.clients.find_one(entry):
    #     mongo.db.clients.insert_one(entry)

    if request.method == 'POST':
        link = request.form.get('url')

        data = func(link)
        session['data'] = data

        return render_template('index2.html', data=data)

    return render_template('index.html')


@app.route("/add_item", methods=['POST'])
def add_item():
    if request.method == 'POST':
        # check session logged in or not
        if 'user' in session:

            set_price = int(request.form.get("set_price"))
            data = session.get('data', None)
            new_data = Products(
                    flipkart = data['flipkart'],
                    amazon =  data['amazon'],
                    price =  set_price,
                    title =  data['title'],
                    prod_link =  data['prod_link'],
                    user =  session['user']
                )
            new_data.save()

            # mongo.db.products.insert_one(new_data)
            return redirect('/profile')

        # if user adding data without login/session
        global is_adding
        is_adding = True
        return redirect('/login')


@app.route('/delete/<string:_id>', methods=['POST'])
def delete(_id):
    if 'user' in session:
        Products.objects(_id = ObjectId(_id)).delete()
        return redirect('/dashboard')
        
    if 'admin' in session and session['admin'] == params['admin_user']:
        Products.objects(_id = ObjectId(_id)).delete()
        return redirect('/admin_panel')


@app.route('/dashboard')
def dashboard():
    if 'user' in session:
        return redirect('/profile')
    return redirect('/login')


@app.route("/login", methods=['GET', 'POST'])
def login():
    if 'user' in session:
        return redirect('/profile')
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        data = Users.objects.get(email = email)
        if not data:
            error = "User does not exist."
            return render_template('sign_in.html', error=error)
        oldpass = data.password
        if bcrypt.check_password_hash(oldpass, password):            
            # here we created a session after logged  in
            session['user'] = email
            global is_adding
            if is_adding:
                data = session.get('data', None)
                return render_template('index2.html', data=data)
            return redirect('/profile')
        error = 'Password not matched.'
        return render_template('sign_in.html', error=error)

    return render_template('sign_in.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'POST':

        name = request.form.get('name')
        phone = request.form.get('phone')
        email = request.form.get('email')
        password = request.form.get('password')

        if Users.objects(email = email):
            error = 'User already exists with this email.'
            return render_template('sign_up.html',error = error)
            
        else:
            hashed = bcrypt.generate_password_hash(password).decode('utf-8')
            # print(hashed)

            session['name'] = name
            session['email'] = email
            session['phone'] = phone
            session['password'] = hashed

            otp = randint(1000, 9999)
            otp = str(otp)
            res = send_mail(email, name, otp)
           
            session['otp'] = otp

            return render_template('verify_otp.html', email=email)
        
    return render_template('sign_up.html')


@app.route("/verify_otp", methods=['POST'])
def verify_otp():
    if request.method == "POST":
        v_otp = request.form.get('otp')
        otp = session.get('otp', None)
        if otp == v_otp:
            
            user_data = Users(
                name = session.get('name', None),
                email =  session.get('email', None),
                phone =  session.get('phone', None),
                password =  session.get('password', None)
            )
            user_data.save()
            
            
            # mongo.db.users.insert_one(user_data)
            # print('entry done')
            email = session.get('email', None)

            # here we create a session after register
            session['user'] = email
            # print('session created')
            global is_adding
            if is_adding:
                return render_template('index2.html', data=session.get('data', None))
            return redirect('/dashboard')
        else:
            email = session.get('email', None)
            message = "OTP not matched. Please enter again."
            return render_template('verify_otp.html', email=email, message=message)
    # return "your account has been created."


@app.route('/profile')
def profile():
    if 'user' in session:
        user = session['user']

        user_data = Users.objects.get(email = user)
        products = Products.objects(user = user)

        for product in products:
            if product.amazon:
                product.prod_link = product.prod_link + "/?tag=gkrbdy-21"
        return render_template('profile.html', user_data=user_data, products=products)

    return redirect('/login')


@app.route('/logout')
def logout():
    global is_adding
    is_adding = False
    session.clear()
    return redirect('/')

@app.route('/contact', methods=['POST'])
def contact():
    name = request.form.get('name')
    email = request.form.get('email')
    phone = request.form.get('phone')
    message = request.form.get('message')

    Contact(name  = name , email = email, phone = phone, message = message).save()

    response = contact_mail(email,name)
    # print(response)
    resp_message = "Thanks for contacting us."
    
    return render_template('index.html',resp_message=resp_message)
@app.route('/deals')
def deals():
    products = Deals.objects()
    return render_template('deals.html',products=products)

@app.route("/admin_login",methods=['GET','POST'])
def admin_login():
    if request.method=="POST":
        username = request.form.get('username')
        password = request.form.get('password')
        if username == params['admin_user'] and password == params['admin_pass']:
            session['admin'] = username
            return redirect('/admin_panel')
        else:
            error = 'Wrong credentials'
            return render_template('adminlogin.html',error = error)
    
    return render_template('adminlogin.html')

@app.route('/admin_panel')
def admin():
    if 'admin' in session and session['admin'] == params['admin_user']:

        users = Users.objects()
        count_users = len(users)

        deal_products = Deals.objects()
        count_deals = len(deal_products)

        all_products = Products.objects()
        count_products = len(all_products)
        count = {
            'users':count_users,
            'deals': count_deals,
            'products': count_products
        }

        return render_template('admin.html',users = users, deal_products= deal_products, products = all_products,count = count)
    else:
        return render_template('adminlogin.html')

@app.route('/add_deals',methods = ['GET','POST'])
def add_deal():
    if 'admin' in session and session['admin'] == params['admin_user']:
        if request.method == 'POST':
            name = request.form.get('name')
            price = request.form.get('price')
            link = request.form.get('link')
            img_link = request.form.get('img_link')
            service = request.form.get('service')
            # print(service)
            if service == "Amazon":
                amazon = True
                flipkart = False
            else:
                flipkart = True
                amazon = False

            add_deals = Deals(name = name, price = price,link = link,img_link = img_link, amazon = amazon, flipkart=flipkart)
            add_deal.save()
            return redirect('/admin_panel')

        return render_template('add_deals.html')
    return render_template('adminlogin.html')

@app.route('/delete-deals/<string:_id>', methods=['POST'])
def delete_deals(_id):
    if 'admin' in session and session['admin'] == params['admin_user']:
        Deals.objects(_id = ObjectId(_id)).delete()
        return redirect('/admin_panel')

@app.route('/forget_pass',methods=['GET','POST'])
def forget_pass():

    if request.method == 'POST':
        email = request.form.get('email')

        check_user = Users.objects.get(email=email)
        if check_user:
            otp_f = str(randint(1000,9999))
            res = send_mail(email,check_user.name, otp_f)
            session['otp_f'] = otp_f
            session['email'] = email
            
            otpsent = True
            return render_template('forgetpass.html',otpsent = otpsent,mail= check_user['email'])

        error1 = 'User does not exist.'
        return render_template('forgetpass.html',error1=error1)
    
    otpsent = False    
    return render_template('forgetpass.html',otpsent=otpsent)

@app.route('/forget_pass1',methods=['POST'])
def forgetpass1():
    otp = request.form.get('otp')
    if otp == session['otp_f']:
        password = request.form.get('password')
        hashed = bcrypt.generate_password_hash(password).decode('utf-8')
        email = session['email']

        Users.objects(email= email).update(password = hashed)
        # mongo.db.users.find_one_and_update({'email':email},{'$set':{'password':hashed}})
        session['user'] = email
        global is_adding
        if is_adding:
            return render_template('index2.html', data=session.get('data', None))
        return redirect('/dashboard')
    error = "OTP did not match."
    otpsent = True
    email = session['email']
    return render_template('forgetpass.html',error2=error,otpsent = otpsent,mail = email)

@app.route('/logs')
def logs():
    with open('logs.txt','r') as logs:
        data = logs.read()
        logs.close()
    return render_template('logs.html',data = data)

# if __name__ == '__main__':
#     app.run(debug=True,host="0.0.0.0",port=5000)


# https://graph.keepa.com/pricehistory.png?asin=B07NSLWJLC&domain=in&range=180
# https://graph.keepa.com/pricehistory.png?asin=B00CSS90ZI&domain=in&width=800&height=250&amazon=1&new=1&used=1&salesrank=1&range=31&cBackground=d7f3fc&cFont=000000&cAmazon=ffba63&cNew=8888dd&cUsed=ffffff
# https://graph.keepa.com/pricehistory.png?asin=B08697N43G&domain=in&width=800&height=250&range=180&cBackground=d7f3fc&cFont=000000
# ?tag=gkrbdy-21

