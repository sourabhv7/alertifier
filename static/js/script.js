function validateForm() {  
    //collect form data in JavaScript variables  
    var pw1 = document.getElementById("password").value;  
    var pw2 = document.getElementById("confirm_password").value;  

    //minimum password length validation  
    if(pw1.length < 8) {  
      document.getElementById("message1").innerHTML = "**Password length must be atleast 8 characters";  
      return false;
    }  
  
    //maximum length of password validation  
    if(pw1.length > 15) {  
      document.getElementById("message1").innerHTML = "**Password length must not exceed 15 characters";  
      return false;
    }  
    
    if(pw1 != pw2) {  
      document.getElementById("message2").innerHTML = "**Passwords are not same";  
      return false;
    } else {  
      // document.getElementById('submit').disabled = false;
      return true;
      //   alert ("Your password created successfully");  
      //   document.write("JavaScript form has been submitted successfully");  
    }  
  }
  
  // Compare two input fields in js
  function compare_input(){
    var f_input= parseInt(document.getElementById('price').textContent);
    console.log(f_input);
    var s_input=document.getElementById('set_price').value;
    console.log(s_input);
    if(f_input>s_input){
      return true;
    }
    else{
      document.getElementById("message").innerHTML = "*Price should be smaller than current price";  
      return false;
      
  }
}