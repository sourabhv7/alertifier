# Bot for tracking the price of products, which stored by users in database. 
# It runs 24x7. It has 2 minutes interval in between two products tracking, and it wait for 5 minutes
# after all products traced and agaain after 5 minutes it repeates the step continuously.


import smtplib
import json
import pymongo
import bs4
import urllib.request
from bson.objectid import ObjectId
import time
import requests
import random
from datetime import datetime
from dateutil.tz import gettz
import urllib.parse
from models import Users, Products
from mongoengine import connect



def short_url(link):
    datab = {
    'url': link
    }
    url = "https://hideuri.com/api/v1/shorten"
    res = requests.post(url,data=datab)
    res = res.json()
    shorten_url = res['result_url']
    return shorten_url

def get_price(soup):
    try:
        prices = soup.find(id="priceblock_ourprice").get_text()
        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        
    except:
        pass

    try:
        prices = soup.find(id="priceblock_dealprice").get_text()
        #print(prices)
        # print('by priceblock_dealprice')

        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        #
    except:
        pass
    try:
        prices = soup.find(id="priceblock_saleprice").get_text()
        # print('by priceblock_saleprice')
        price = float(prices.replace(",", "").replace("₹", ""))
        return price
        #
    except:
        price = 0
        # print(price)
        return price

def func(link):

    ran_v = random.uniform(1.0,10.0)
    ran_v = str(ran_v)
    header = {'User-Agent': f'Mozilla/{ran_v}'}
    req = urllib.request.Request(link, headers=header)
    resp = urllib.request.urlopen(req)
    data = resp.read()
    sauce = data.decode("UTF-8")
    soup = bs4.BeautifulSoup(sauce, "html.parser")
    if 'amazon' in link:
        price = get_price(soup)
        #print(price)

    elif 'flipkart' in link:
        price = soup.find(class_="_30jeq3 _16Jk6d").get_text()
        price = float(price .replace("₹", "").replace(",", ""))
        #print(price)

    return int(price)



def send_mail(to, name,link,prod_name):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    try:
        msg = f"""From: Alertifier <alertifier.ml@gmail.com>
To: {name}<{to}>
Subject: Product Price Dropped       
Hi {name[0]},
Your product's ({prod_name}) price has been dropped now. Buy it quickly {link}"""

        server.starttls()
        server.login('alertifier.ml@gmail.com', params['gmail_pass'])
        server.sendmail('alertifier.ml@gmail.com', to, msg)
        # getting response after sending the mail
        response = {
            'status': 'success',
            'from': 'alertifier.ml@gmail.com',
            'to': to,
            'message': msg
        }
        return response
    except:
        response = {
            'status': 'failed'
        }
        return response

def sendsms(payload):
    
    reqUrl = "https://www.fast2sms.com/dev/bulkV2"

    headersList = {
        "authorization": "OFmZKWj0MEcG8wdr9hSzv7UoCq6aQIbfe1RHDkuPXpVlBs3xJAP9m0HvjNDwdWstzlBbXZcEK3qenT1U",
        "Content-Type": "application/json"
    }
    response = requests.request(
        "POST", reqUrl, data=payload,  headers=headersList)
    return response

with open('config.json', 'r') as c:
    params = json.load(c)["params"]

local_server = False

if local_server:
    uri = params['local_uri']
else:
    uri = params['prod_uri']


# Connecting mongoengine with mongoDB server instance
connect(host=uri)


def main():
    data = Products.objects()
    for item in data:
        prod_link = item.prod_link
        # print(prod_link)
        price  = item.price
        print(price)
        flipkart = item.flipkart
        amazon = item.amazon
        new_price = func(prod_link)
        print(new_price)
        flag = False
        if new_price<=int(price) and new_price != 0:
            if amazon:
                prod_name = item.title
                email = item.user

                user_data = Users.objects.get(email = email)
                name = userdata.name
                phone = userdata.phone
                prod_link1 = prod_link+ "/?tag=gkrbdy-21"
                status = 'failed'
                while status == 'failed':
                    response = send_mail(email,name,prod_link1,prod_name)
                    status = response.status

                var1 = ""
                var2 = ""
                if len(prod_link1) > 30:
                    var1 = prod_link1[0:30]
                    var2 = prod_link1[30:len(prod_link1)]
                else:
                    var1 = prod_link1

                var_values = f"{var1}|{var2}|"

                payload = "{\r\n\"route\" : \"dlt\",\r\n\"sender_id\" : \"SOURBV\",\r\n\"message\" : \"131605\",\r\n\"variables_values\" : \"var_values\",\r\n\"flash\" : 0,\r\n\"numbers\" : \"8878027995\"\r\n}".replace("var_values", var_values)
                sresponse = sendsms(payload)
                print(sresponse.text)
                resp = sresponse.text
            
                Deals.objects(user = email, prod_link = prod_link).delete()
                
                flag = True
                print(response)
                print(resp)

            elif flipkart:
                prod_name = item.title
                email = item.user

                user_data = Users.objects.get(email = email)

                name = userdata.name
                phone = userdata.phone
                prod_link1 = short_url(prod_link)
                print(prod_link1)
                status = 'failed'
                while status == 'failed':
                    response = send_mail(email,name,prod_link1,prod_name)
                    status = response['status']
                print(response)

                var1 = ""
                var2 = ""
                if len(prod_link1) > 30:
                    var1 = prod_link1[0:30]
                    var2 = prod_link1[30:len(prod_link)]

                else:
                    var1 = prod_link1

                var_values = f"{var1}|{var2}|"
                payload = "{\r\n\"route\" : \"dlt\",\r\n\"sender_id\" : \"SOURBV\",\r\n\"message\" : \"131605\",\r\n\"variables_values\" : \"var_values\",\r\n\"flash\" : 0,\r\n\"numbers\" : \"8878027995\"\r\n}".replace("var_values", var_values)
                sresponse = sendsms(payload)
                print(sresponse.text)
                resp = sresponse.text
                Deals.objects(user = email, prod_link = prod_link).delete()

                flag = True

        t = datetime.now(tz=gettz('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        t = str(t)
        with open('logs.txt','a') as log:
            log.write(t)
            log.write('-->')
            log.write(item['title'][0:40])
            log.write('-->')
            log.write(str(new_price))
            if flag:
                log.write(json.dumps(response))
                log.write('-->')
                log.write(str(resp))
            log.write('\n')
            log.close()
        time.sleep(120)

if __name__ == '__main__':
    while True:
        try:
            main()
        except Exception  as e:
            print(e)
            with open('logs.txt','a') as log:
                log.write(str(e))
                log.write('\n')
                log.close()
        time.sleep(600)
    

     



