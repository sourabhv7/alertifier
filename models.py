from mongoengine import Document, StringField, IntField, EmailField , URLField, ObjectIdField, BooleanField

# connect(db="testmongo", host = "localhost", port = 27017)


class Users(Document):
    _id = ObjectIdField()
    name = StringField()
    email = EmailField()
    phone = StringField()
    password = StringField()


class Clients(Document):
    _id = ObjectIdField()
    ip = StringField()
    header = StringField()

class Products(Document):
    _id = ObjectIdField()
    amazon = BooleanField()
    flipkart = BooleanField()
    price = IntField()
    title = StringField()
    prod_link = URLField()
    user = EmailField()

class Contact(Document):
    name = StringField()
    email = EmailField()
    phone = StringField()
    message = StringField()

class Deals(Document):
    _id = ObjectIdField()
    name = StringField()
    price = StringField()
    link = URLField()
    img_link = URLField()
    amazon = BooleanField()
    flipkart = BooleanField()


     